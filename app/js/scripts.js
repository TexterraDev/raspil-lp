$( document ).ready(function() {

	// Our work swiper-slider
	var swiper = new Swiper('.our__work__slider', {
		slidesPerView: 'auto',
    scrollbar: {
      el: '.swiper-scrollbar',
      hide: false,
      draggable: true,
      dragSize: 71
    },
  });

  $('.calculator__form__select').selectmenu();

  $('.calculator__form__input.text').on('input', function (event) { 
	  this.value = this.value.replace(/[^0-9]/g, '');
	});

	$('.faq__block__item__question').click(function() {
		$(this).toggleClass('active');
		$(this).next('.faq__block__item__answer').slideToggle();
	});

	// Pop-up callbacks

	$('.btn__lp').click(function(event){
		event.preventDefault();
		$('.underlay').addClass('opened');
		if ($(this).hasClass('top-callback')) {
			$('.pop__up.top-callback').fadeIn();
		} else if ($(this).hasClass('calculator__form__submit')) {
			$('.pop__up.calc-request').fadeIn();
		}
	});

	$('.pop__up__close').click(function(){
		$(this).parent('.pop__up').fadeOut();
		$('.underlay').removeClass('opened');
	});

	$('.services__list__item').click(function() {
		$('.underlay').addClass('opened');
		$('.pop__up.product').fadeIn();
	});

	$('.bottom__request__form__input[type="tel"], .pop__up__form__input[type="tel"]').inputmask({"mask": "+7 (999) 999-99-99"});

	var headerHeight = $('.header__lp').outerHeight();
	$(window).scroll(function() {
		if ($(this).scrollTop() > headerHeight) {
			$('.to__top').addClass('fixed');
		} else {
			$('.to__top').removeClass('fixed');
		}
	});

	$('.to__top').click(function(toTop){
		event.preventDefault(toTop);
		$('body,html').animate({scrollTop: 0}, 1000);
    return false;
	});

});

function showBottomThanks() {
	$('.bottom__request .container').children().not('.bottom__request__thanks').fadeOut();
	$('.bottom__request__thanks').fadeIn();
}